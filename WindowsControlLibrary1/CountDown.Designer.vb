﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CountDown
    Inherits System.Windows.Forms.UserControl

    'UserControl 重写释放以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PictureBox_about = New System.Windows.Forms.PictureBox()
        Me.Timer_countdown = New System.Windows.Forms.Timer(Me.components)
        Me.Label_countdown = New System.Windows.Forms.Label()
        CType(Me.PictureBox_about, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox_about
        '
        Me.PictureBox_about.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox_about.Image = Global.ZEE.USERCONTROL.My.Resources.Resources._20100203
        Me.PictureBox_about.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox_about.Name = "PictureBox_about"
        Me.PictureBox_about.Size = New System.Drawing.Size(184, 77)
        Me.PictureBox_about.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox_about.TabIndex = 8
        Me.PictureBox_about.TabStop = False
        Me.PictureBox_about.Visible = False
        '
        'Label_countdown
        '
        Me.Label_countdown.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label_countdown.AutoSize = True
        Me.Label_countdown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label_countdown.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_countdown.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label_countdown.Location = New System.Drawing.Point(20, 10)
        Me.Label_countdown.Name = "Label_countdown"
        Me.Label_countdown.Size = New System.Drawing.Size(78, 55)
        Me.Label_countdown.TabIndex = 9
        Me.Label_countdown.Text = "36"
        Me.Label_countdown.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CountDown
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label_countdown)
        Me.Controls.Add(Me.PictureBox_about)
        Me.Name = "CountDown"
        Me.Size = New System.Drawing.Size(184, 77)
        CType(Me.PictureBox_about, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox_about As PictureBox
    Friend WithEvents Timer_countdown As Timer
    Friend WithEvents Label_countdown As Label
End Class
