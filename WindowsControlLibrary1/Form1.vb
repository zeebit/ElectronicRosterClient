﻿Public Class Form1
    Private ZZmouse_down As Boolean = False
    Private ZZmouse_point As Point
    Private ZZmouse_point_as_screen As Point
    Public ini_file As String = Application.StartupPath & "\CountDown.ini"
    Public zztime
    Public zztime_Differ As Integer
    Public zzimage() As Image = {My.Resources.CountDown_0, My.Resources.CountDown_1, My.Resources.CountDown_2, My.Resources.CountDown_3, My.Resources.CountDown_4, My.Resources.CountDown_5, My.Resources.CountDown_6, My.Resources.CountDown_7, My.Resources.CountDown_8, My.Resources.CountDown_9}


    Private Sub Form_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown, PictureBox1.MouseDown, PictureBox2.MouseDown
        Me.ZZmouse_down = True
        Me.ZZmouse_point = New Point(e.X, e.Y)
    End Sub
    Private Sub Form_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove, PictureBox1.MouseMove, PictureBox2.MouseMove
        ZZmouse_point_as_screen = New Point(e.X, e.Y)
        ZZmouse_point_as_screen = New Point(e.X, e.Y)
        If Me.ZZmouse_down = True Then
            Me.Location = PointToScreen(ZZmouse_point_as_screen - Me.ZZmouse_point)
        End If
    End Sub
    Private Sub Form_MouseUp(sender As Object, e As MouseEventArgs) Handles Me.MouseUp, PictureBox1.MouseUp, PictureBox2.MouseUp
        Me.ZZmouse_down = False
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        'zztime = "15:37"
        Me.Height = 98
        Me.Width = 169
        Label1.Text = ""
        For j = 1 To 100
            Dim jjj = INI_GetStr("setup", "t" & j, "", ini_file)
            If jjj <> "" Then
                ContextMenuStrip1.Items.Add(jjj)
            Else
                Exit For
            End If
        Next
        Dim aa As Integer = ContextMenuStrip1.Items.Count - 1
        For i = 4 To aa
            Console.WriteLine("---------------------" & i & "--" & ContextMenuStrip1.Items(i).Text)
            AddHandler ContextMenuStrip1.Items(i).Click, AddressOf ComMouseClicked
        Next
    End Sub



    Private Sub ComMouseClicked(ByVal sender As Object, ByVal e As System.EventArgs)
        Console.WriteLine("菜单-----------" & sender.ToString)
        TimeToGo(Split(sender.ToString(), "-")(1))
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        zztime_Differ = DateDiff("n", Now, zztime) 's=second  n=minute
        Console.WriteLine(zztime_Differ)

        If zztime_Differ < 100 Then
            If zztime_Differ >= 10 Then
                PictureBox1.Image = zzimage(Mid(zztime_Differ, 1, 1))
                PictureBox2.Image = zzimage(Mid(zztime_Differ, 2, 1))
            ElseIf zztime_Differ < 10 And zztime_Differ > 0 Then
                PictureBox1.Image = Nothing
                PictureBox2.Image = zzimage(Mid(zztime_Differ, 1, 1))
            ElseIf zztime_Differ = 0 Then
                '开始计秒
                Dim zzss = DateDiff("s", Now, zztime) 's=second  n=minute
                Console.WriteLine("zzss=" & zzss)
                If zzss >= 10 Then
                    PictureBox1.Image = zzimage(Mid(zzss, 1, 1))
                    PictureBox2.Image = zzimage(Mid(zzss, 2, 1))
                Else
                    Try
                        PictureBox1.Image = Nothing
                        PictureBox2.Image = zzimage(Mid(zzss, 1, 1))
                    Catch ex As Exception
                    End Try
                End If
            Else
                '倒计时完成
                PictureBox1.Image = My.Resources.CountDown_done
                PictureBox2.Image = My.Resources.CountDown_done
                Timer1.Enabled = False
            End If
        Else
            PictureBox1.Image = My.Resources.CountDown__
            PictureBox2.Image = My.Resources.CountDown__
        End If
    End Sub

    Private Sub ToolStripMenuItem8_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem8.Click
        Me.Opacity = 1
    End Sub

    Private Sub ToolStripMenuItem9_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem9.Click
        Me.Opacity = 0.9
    End Sub

    Private Sub ToolStripMenuItem10_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem10.Click
        Me.Opacity = 0.8
    End Sub

    Private Sub ToolStripMenuItem11_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem11.Click
        Me.Opacity = 0.7
    End Sub

    Private Sub ToolStripMenuItem12_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem12.Click
        Me.Opacity = 0.6
    End Sub

    Private Sub ToolStripMenuItem13_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem13.Click
        Me.Opacity = 0.5
    End Sub

    Private Sub 退出ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles 退出ToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub 关于ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles 关于ToolStripMenuItem.Click
        PictureBox_about.Show()
        PictureBox_about.BringToFront()
    End Sub

    Private Sub ToolStripTextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ToolStripTextBox1.KeyPress
        If e.KeyChar = ChrW(13) Then '回车
            TimeToGo(ToolStripTextBox1.Text)
        End If
    End Sub

    Private Sub ContextMenuStrip1_Opened(sender As Object, e As EventArgs) Handles ContextMenuStrip1.Opened
        '当右键菜单打开时
        ToolStripTextBox1.Text = DateTime.Now.ToString("HH:mm")
        'Dim b As FontStyle = FontStyle.Bold
        'Dim nn As FontStyle = FontStyle.Regular
        For i = 4 To ContextMenuStrip1.Items.Count - 1
            Dim zztttime = DateTime.Now.ToString("yyyy/MM/dd") & " " & Split(ContextMenuStrip1.Items(i).Text, "-")(1) & ":00"
            Dim zz_Differ = DateDiff("n", Now, zztttime) 's=second  n=minute
            If zz_Differ > 1 And zz_Differ < 50 Then
                ContextMenuStrip1.Items(i).Font = New Font("微软雅黑", 9, FontStyle.Bold)
            Else
                ContextMenuStrip1.Items(i).Font = New Font("微软雅黑", 9, FontStyle.Regular)
            End If
        Next
    End Sub

    Sub TimeToGo(zzMenuText)
        zztime = DateTime.Now.ToString("yyyy/MM/dd") & " " & zzMenuText & ":00"
        'zztime = "2019/9/21 15:37:00"
        Label1.Text = zzMenuText
        Timer1.Interval = 50
        Timer1.Enabled = True
    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox_about.Click
        PictureBox_about.Hide()
    End Sub
End Class
