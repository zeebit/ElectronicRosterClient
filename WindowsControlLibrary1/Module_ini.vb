﻿Option Explicit On
Module Module_ini
    'INICont.bas Ver 1.0+a  INI    
    '====================================================================
    'GetIntFromINI( sectionName , keyName , defaultValue, iniPath )
    '          sectionName      :节点名
    '          keyName          :配置项名
    '          defaultValue     :默认值
    '          iniPath          :INI配置文件的路径
    ' INI_WriteStr("config", "zStart", "bbb", Application.StartupPath & "\config.ini")
    ' TextBox_1.Text = INI_GetInt("config", "zStart", Lb_url_num.Text, Application.StartupPath & "\config.ini") 'defaultValue参数要了干啥？？
    '====================================================================
    '声明从INI配置文件中获取类型为Int的配置项的值的系统函数
    Private Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal nDefault As Integer, ByVal lpFileName As String) As Integer
    '声明从INI配置文件中获取类型为string的配置项的值的系统函数
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    '声明向INI配置文件中写入类型为string的配置项的值的系统函数
    Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer

    '从INI配置文件中获取类型为Int的配置项的值
    Public Function INI_GetInt(ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As Integer, ByVal iniPath As String) As Integer
        INI_GetInt = GetPrivateProfileInt(sectionName, keyName, defaultValue, iniPath)
    End Function

    '从INI配置文件中获取类型为string的配置项的值
    Public Function INI_GetStr(ByVal sectionName As String, ByVal keyName As String, ByVal defaultValue As String, ByVal iniPath As String) As String
        Dim buffer As String
        Dim rc As Integer
        buffer = Space(256)
        rc = GetPrivateProfileString(sectionName, keyName, defaultValue, buffer, buffer.Length, iniPath)
        INI_GetStr = Left(buffer, InStr(buffer, vbNullChar) - 1)
    End Function

    '向INI配置文件中写入类型为string的配置项的值
    Public Function INI_WriteStr(ByVal sectionName As String, ByVal keyName As String, ByVal setValue As String, ByVal iniPath As String) As Integer
        Dim rc As Integer
        rc = WritePrivateProfileString(sectionName, keyName, setValue, iniPath)
        If rc Then
            rc = 1
        End If
        INI_WriteStr = rc
    End Function
End Module
