﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' 有关程序集的一般信息由以下
' 控制。更改这些特性值可修改
' 与程序集关联的信息。

'查看程序集特性的值

<Assembly: AssemblyTitle("ElectronicRosterClient")>
<Assembly: AssemblyDescription("电子名单生成客户端")>
<Assembly: AssemblyCompany("ZEEBIT@QQ.COM")>
<Assembly: AssemblyProduct("ElectronicRosterClient")>
<Assembly: AssemblyCopyright("©2019 ZEEBIT@QQ.COM")>
<Assembly: AssemblyTrademark("ZEEBIT@QQ.COM")>

<Assembly: ComVisible(False)>

'如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
<Assembly: Guid("00a36737-d72e-4215-ab77-994b3abd04e3")>

' 程序集的版本信息由下列四个值组成: 
'
'      主版本
'      次版本
'      生成号
'      修订号
'
' 可以指定所有值，也可以使用以下所示的 "*" 预置版本号和修订号
' 方法是按如下所示使用“*”: :
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.0.0")>
<Assembly: AssemblyFileVersion("2.0.0.0")>
