﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Text

Public Class main
    Private ZZmouse_down As Boolean = False
    'Public tempppppp As String
    Private ZZmouse_point As Point
    Private ZZmouse_point_as_screen As Point
    Dim MyAppPath = Application.StartupPath             '获取本程序路径 My.Application.Info.DirectoryPath 这个在win7下正常，但XP下为空
    Dim MyAppFilename = System.IO.Path.GetFileNameWithoutExtension(Application.ExecutablePath) '获取本程序文件名
    Dim MyPcname As String
    Dim fontFamilies() As FontFamily '创建一个字体数组
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'AddHandler PictureboxButton1.Click, AddressOf PictureboxButton1_Click
        'AddHandler PictureboxButton2.Click, AddressOf PictureboxButton2_Click
        AddHandler PB_close.Click, AddressOf PB_close_Click

        MyPcname = SystemInformation.ComputerName
        'PictureboxButton1.Parent = PB_back
        'PictureboxButton2.Parent = PB_back
        'PictureBox_close.Parent = PB_back
        'Label1.Parent = PB_back
        'Label_tip.Parent = PB_back
        'Label_confirm_name.Parent = PB_back
        'Label_pcname.Parent = PB_back
        Label_pcname.Text = "SERVER: 192.168.1.10   PCNAME: " & MyPcname
        TextBox1.Text = ""
        Label_confirm_name.Visible = False
        Button2.Visible = False

        Dim installedFontCollection As New Drawing.Text.InstalledFontCollection() '枚举系统字体
        fontFamilies = installedFontCollection.Families '系统字体装进字体数组
        Dim kaiti = searchFont("楷体")
        TextBox1.Font = New Font(fontFamilies(kaiti).Name, 36)
        Label_confirm_name.Font = New Font(fontFamilies(kaiti).Name, 36)
        Dim fangsong = searchFont("仿宋")
        Label_tip.Font = New Font(fontFamilies(fangsong).Name, 15)
        Dim dengxian = searchFont("等线")
        If dengxian = 0 Then dengxian = searchFont("宋体") '如果系统没有安装 等线 则设置 宋体
        'ComboBox1.Font = New Font(fontFamilies(dengxian).Name, 12)
        'PictureboxButton1.Font = New Font(fontFamilies(dengxian).Name, 21.75)
        'PictureboxButton2.Font = New Font(fontFamilies(dengxian).Name, 21.75)
        'tempppppp = ""
        If Set_InputLanguage("搜狗") = False Then
            If Set_InputLanguage("拼音") = False Then
                Set_InputLanguage("五笔")
            End If
        End If
        'MsgBox(tempppppp)
        Me.ActiveControl = TextBox1
    End Sub
    Function Set_InputLanguage(zzlayoutname As String) '"搜狗" QQ(区别大小写的）
        Dim bbb As Boolean = False
        For Each aa In System.Windows.Forms.InputLanguage.InstalledInputLanguages
            Debug.Print(aa.layoutname)
            'tempppppp += aa.layoutname & vbCrLf
            If InStr(aa.layoutname, zzlayoutname) Then
                System.Windows.Forms.InputLanguage.CurrentInputLanguage = aa '切换指定输入法
                Debug.Print("选中" & aa.layoutname)
                'tempppppp += "选中=========" & aa.layoutname & vbCrLf
                bbb = True
            End If
        Next
        Return bbb
    End Function
    Function searchFont(fontname As String) As Integer
        '搜索字体是否安装，返回数组fontFamilies序号，=0则未找到
        Dim jj = 0
        For i = 0 To fontFamilies.Length - 1
            If InStr(fontFamilies(i).Name, fontname) Then
                jj = i
            End If
        Next
        Return jj
    End Function



    Private Sub PB_back_MouseDown(sender As Object, e As MouseEventArgs) Handles Panel2.MouseDown, Label1.MouseDown, Me.MouseDown
        Me.ZZmouse_down = True
        Me.ZZmouse_point = New Point(e.X, e.Y)
    End Sub
    Private Sub PB_back_MouseMove(sender As Object, e As MouseEventArgs) Handles Panel2.MouseMove, Label1.MouseMove, Me.MouseMove
        ZZmouse_point_as_screen = New Point(e.X, e.Y)
        If Me.ZZmouse_down = True Then
            Me.Location = PointToScreen(ZZmouse_point_as_screen - Me.ZZmouse_point)
        End If
    End Sub
    Private Sub PB_back_MouseUp(sender As Object, e As MouseEventArgs) Handles Panel2.MouseUp, Label1.MouseUp, Me.MouseUp
        Me.ZZmouse_down = False
    End Sub


    Private Sub PictureboxButton1_Click(ByVal source As Object, ByVal e As EventArgs)
        '在Form_load里要有以下代码行
        '并将PictureboxButton1控件的Click事件与事件处理程序PictureboxButton1_Click连接起来’
        'AddHandler PictureboxButton1.Click, AddressOf roundButton_Click

        '用户控件里，还要加以下代码，  Label在上，Picturebox在下
        'Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click, PictureBox1.Click
        '    MyBase.OnClick(e)
        'End Sub


    End Sub

    Private Sub PB_close_Click(sender As Object, e As EventArgs)
        Me.Close()
        'MyBase.
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        '提交 确认
        If Button1.Text = "确认" Then
            TextBox1.Visible = False
            Label_confirm_name.Visible = True
            TextBox1.Text = TextBox1.Text.Replace(" ", "") '空格替换掉
            Label_confirm_name.Text = TextBox1.Text
            Button1.Text = "提交"
            Button2.Visible = True
            Label_tip.Text = "请检查您的名字是否正确！"
        Else
            Try
                Dim bytes(1024) As Byte
                Dim s = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
                Dim localEndPoint As New IPEndPoint(IPAddress.Parse("192.168.1.10"), 1024)
                s.Connect(localEndPoint)
                s.Send(Encoding.Unicode.GetBytes(MyPcname & TextBox1.Text))
                s.Close()
                Button1.Enabled = False
                Button2.Visible = False
                Label_tip.Text = "提交成功，谢谢您的参与。"
                'ComboBox1.Visible = False
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        TextBox1.Visible = True
        Label_confirm_name.Visible = False
        Button1.Text = "确认"
        Button2.Visible = False
        Label_tip.Text = "请输入您的名字："
        Me.ActiveControl = TextBox1

    End Sub

    Private Sub PB_close_Load(sender As Object, e As EventArgs) Handles PB_close.Load

    End Sub

    Private Sub CountDown1_Load(sender As Object, e As EventArgs) Handles CountDown1.Load

    End Sub
End Class

