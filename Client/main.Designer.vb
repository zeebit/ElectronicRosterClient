﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class main
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label_tip = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label_confirm_name = New System.Windows.Forms.Label()
        Me.Label_pcname = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CountDown1 = New ZEE.USERCONTROL.CountDown()
        Me.PB_close = New ZEE.USERCONTROL.PictureboxButton()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("微软雅黑", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(129, 21)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "电子名单CLIENT"
        '
        'Label_tip
        '
        Me.Label_tip.AutoSize = True
        Me.Label_tip.BackColor = System.Drawing.Color.Transparent
        Me.Label_tip.Font = New System.Drawing.Font("仿宋", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label_tip.Location = New System.Drawing.Point(50, 66)
        Me.Label_tip.Name = "Label_tip"
        Me.Label_tip.Size = New System.Drawing.Size(169, 20)
        Me.Label_tip.TabIndex = 3
        Me.Label_tip.Text = "请输入您的名字："
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("楷体", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(54, 107)
        Me.TextBox1.MaxLength = 10
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(388, 62)
        Me.TextBox1.TabIndex = 7
        Me.TextBox1.Text = "某某某某"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBox1.WordWrap = False
        '
        'Label_confirm_name
        '
        Me.Label_confirm_name.BackColor = System.Drawing.Color.Transparent
        Me.Label_confirm_name.Font = New System.Drawing.Font("楷体", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label_confirm_name.Location = New System.Drawing.Point(59, 107)
        Me.Label_confirm_name.Name = "Label_confirm_name"
        Me.Label_confirm_name.Size = New System.Drawing.Size(388, 62)
        Me.Label_confirm_name.TabIndex = 8
        Me.Label_confirm_name.Text = "aaa"
        Me.Label_confirm_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label_pcname
        '
        Me.Label_pcname.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label_pcname.Font = New System.Drawing.Font("微软雅黑", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label_pcname.ForeColor = System.Drawing.Color.DimGray
        Me.Label_pcname.Location = New System.Drawing.Point(59, 194)
        Me.Label_pcname.Name = "Label_pcname"
        Me.Label_pcname.Size = New System.Drawing.Size(388, 22)
        Me.Label_pcname.TabIndex = 11
        Me.Label_pcname.Text = "Label2"
        Me.Label_pcname.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel2.Controls.Add(Me.Button2)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Location = New System.Drawing.Point(0, 219)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(494, 69)
        Me.Panel2.TabIndex = 17
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("微软雅黑", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Button2.Location = New System.Drawing.Point(230, 16)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(113, 38)
        Me.Button2.TabIndex = 18
        Me.Button2.Text = "重新输入"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("微软雅黑", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Button1.Location = New System.Drawing.Point(360, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(82, 38)
        Me.Button1.TabIndex = 17
        Me.Button1.Text = "确认"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CountDown1
        '
        Me.CountDown1.Location = New System.Drawing.Point(291, 9)
        Me.CountDown1.Name = "CountDown1"
        Me.CountDown1.Size = New System.Drawing.Size(111, 59)
        Me.CountDown1.TabIndex = 19
        '
        'PB_close
        '
        Me.PB_close.BackColor = System.Drawing.Color.Transparent
        Me.PB_close.Location = New System.Drawing.Point(466, -1)
        Me.PB_close.Name = "PB_close"
        Me.PB_close.Size = New System.Drawing.Size(28, 28)
        Me.PB_close.TabIndex = 15
        Me.PB_close.zzImage = Global.ElectronicRosterClient.My.Resources.Resources.c32_1
        Me.PB_close.zzImage_2 = Global.ElectronicRosterClient.My.Resources.Resources.c32_2
        Me.PB_close.zzText = ""
        '
        'main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(494, 286)
        Me.Controls.Add(Me.CountDown1)
        Me.Controls.Add(Me.PB_close)
        Me.Controls.Add(Me.Label_pcname)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label_confirm_name)
        Me.Controls.Add(Me.Label_tip)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "电子名单 - Client"
        Me.TopMost = True
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As Label
    Friend WithEvents Label_tip As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label_confirm_name As Label
    Friend WithEvents Label_pcname As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents PB_close As ZEE.USERCONTROL.PictureboxButton
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents CountDown1 As ZEE.USERCONTROL.CountDown
    'Friend WithEvents ZzPictureboxButton1 As zuctrl.zzPictureboxButton
    'Friend WithEvents UserControl11 As WindowsControlLibrary1.UserControl1
End Class
